﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "MejorVuelta", menuName = "ScriptableObjects/MejorVuelta", order = 3)]
public class MejorVuelta : ScriptableObject
{
    public int minutos;
    public int segundos;
    public float mil;
}
