﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ActivarLuces : MonoBehaviour
{
    private GameObject coche;
    private float distance;
    private GameObject precaucions;


    // Start is called before the first frame update
    void Start()
    {
        coche = GameObject.Find("Coche");
        precaucions = GameObject.Find("TextoSOS");
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(coche.transform.position, this.transform.position);
        if(distance < 30f)
        {
            this.transform.GetChild(0).gameObject.SetActive(true);
                    if (coche.GetComponent<CarController2>().velocidad > 120)
                      {
                     StartCoroutine(Precation());
                      }
        }
        else
        {
            this.transform.GetChild(0).gameObject.SetActive(false);
        }
    }
    IEnumerator Precation()
    {
        precaucions.GetComponent<TextMeshProUGUI>().enabled = true;
        yield return new WaitForSeconds(.3f);
        precaucions.GetComponent<TextMeshProUGUI>().enabled = false;
    }
}
