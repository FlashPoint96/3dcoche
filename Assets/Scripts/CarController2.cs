﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController2 : MonoBehaviour
{
    public CarSettings CS;
    private bool isBreaking;
    public float velocidad;
    public GameObject humo;
    public TrailRenderer[] TiresMarks;
    private bool TMarks;

    public WheelCollider[] ruedas;
    public Transform[] transformruedas;

    public Material luzroja;
    public Material luzblanca;
    private Material[] m_cosascoche;
    private MeshRenderer mr;

    public GameEvent EmpezarVuelta;
    public GameEvent AcabarVuelta;

    // Start is called before the first frame update
    void Start()
    {
        mr = this.GetComponentInChildren<MeshRenderer>();
        m_cosascoche = mr.materials;
        luzblanca = m_cosascoche[1];
        TMarks = true;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Moverse();
        UpdateWheels();
        Breaking();
    }

    private void Breaking()
    {
        if (isBreaking){
            this.GetComponent<Rigidbody>().drag = 1;
            m_cosascoche[1] = luzroja;
            mr.materials = m_cosascoche;
            TMarks = true;
            WheelFrictionCurve frictionCurve = ruedas[0].sidewaysFriction;
            frictionCurve.stiffness = 1f;
            foreach (WheelCollider wc in ruedas)
            {
                wc.brakeTorque = CS.BrakeForce;
                wc.sidewaysFriction = frictionCurve;
            }
            Derrape();
        }
        else
        {
            this.GetComponent<Rigidbody>().drag = 0;
            m_cosascoche[1] = luzblanca;
            mr.materials = m_cosascoche;
            DejarDeDerrapar();

        }
    }

        private void Derrape()
        {
            if (TMarks)
            {
                foreach (TrailRenderer TR in TiresMarks)
                {
                    TR.emitting = true;
                    if (TR.name.Equals("TR1") || TR.name.Equals("TR2"))
                    {
                        Instantiate(humo, TR.transform.position, Quaternion.identity);
                    }
                }
            }
            TMarks = false;
        }

    private void DejarDeDerrapar()
    {
        if (!TMarks)
        {
            foreach (TrailRenderer TR in TiresMarks)
            {
                TR.emitting = false;
            }
        }
        TMarks = false;
    }
    private void UpdateWheels()
    {
        for (int i = 0; i < ruedas.Length; i++)
        {
            UpdateWheelPos(ruedas[i], transformruedas[i].transform);
        }
    }
    private void UpdateWheelPos(WheelCollider wheelCollider, Transform wheelTransform)
    {

        Vector3 pos;
        Quaternion rot;
        wheelCollider.GetWorldPose(out pos, out rot);
        wheelTransform.rotation = rot;
        wheelTransform.position = pos;
    }
    private void Moverse()
    {
        ruedas[0].steerAngle = CS.SteeringAngle * Input.GetAxis("Horizontal");
        ruedas[1].steerAngle = CS.SteeringAngle * Input.GetAxis("Horizontal");
        isBreaking = Input.GetKey(KeyCode.Space);
        velocidad = this.GetComponent<Rigidbody>().velocity.magnitude * 3.6f;
        if (velocidad < 180f)
        {
            this.GetComponent<Rigidbody>().drag = 0;
            foreach (WheelCollider wc in ruedas)
            {
                wc.motorTorque = Input.GetAxis("Vertical") * CS.MotorForce;
            }
            if (velocidad > 120)
            {
                this.GetComponent<Rigidbody>().drag = 1f;
                WheelFrictionCurve frictionCurve = ruedas[0].sidewaysFriction;
                frictionCurve.stiffness = 0.6f;
                frictionCurve.extremumValue = 1;
                frictionCurve.extremumSlip = 0.2f;
                frictionCurve.asymptoteSlip = 0.5f;
                frictionCurve.asymptoteValue = 0.75f;
                foreach (WheelCollider wc in ruedas)
                {
                    wc.sidewaysFriction = frictionCurve;
                }
            }
            else
            {
                WheelFrictionCurve frictionCurve = ruedas[0].sidewaysFriction;
                frictionCurve.stiffness = .8f;
                foreach (WheelCollider wc in ruedas)
                {
                    wc.brakeTorque = 0;
                    wc.sidewaysFriction = frictionCurve;
                }
            }
        }
        else
        {
            this.GetComponent<Rigidbody>().drag = 0.5f;
            foreach (WheelCollider wc in ruedas)
            {
                wc.motorTorque = 0;
                wc.brakeTorque = 0;
            }
        }
        CS.BrakeForce = isBreaking ? 5000f : 0f;
    }




    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "EmpezarVuelta")
        {
            EmpezarVuelta.Raise();
        }
        if (other.gameObject.name == "AcabarVuelta")
        {
            AcabarVuelta.Raise();
        }
    }

}
