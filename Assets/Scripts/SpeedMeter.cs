﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class SpeedMeter : MonoBehaviour
{
    public GameObject aguja;
    public GameObject objetotexto;
    private TextMeshProUGUI texto;
    public GameObject coche;
    public float velocidadcoche;
    private float pos0 = 216;
    private float pos1 = -40;
    private float posactual;

    public static int numerovueltas;
    public GameObject vueltas;
    // Update is called once per frame
    void FixedUpdate()
    {
        velocidadcoche = coche.GetComponent<CarController2>().velocidad;
        ActualizarAguja();
        vueltas.GetComponent<TextMeshProUGUI>().text = numerovueltas.ToString() + "/3";
        if (numerovueltas == 3)
        {
            numerovueltas = 0;
            SceneManager.LoadScene("Tremendo");
        }
    }

    private void ActualizarAguja()
    {
        texto = objetotexto.GetComponent<TextMeshProUGUI>();
        texto.text = Mathf.Round(velocidadcoche) + "km/h";
        posactual = pos0 - pos1;
        float newpos = velocidadcoche / 260;
        aguja.transform.eulerAngles = new Vector3(0, 0,(pos0 - newpos * posactual));
     }

}
