﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "LightSettings", menuName = "ScriptableObjects/LightSettings", order = 2)]
public class LightSettings : ScriptableObject
{
    public float LongLightIntensity;
    public float LongLightRange;
    public float LongLightSpotRange;

    public float ShortightIntensity;
    public float ShortLightRange;
    public float ShortLightSpotRange;
}
