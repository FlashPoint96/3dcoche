﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "CarSettings", menuName = "ScriptableObjects/CarSettings", order = 1)]
public class CarSettings : ScriptableObject
{
    public float MotorForce;
    public float SteeringAngle;
    public float BrakeForce;
}
