﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour
{
    public GameObject LongLights;
    public GameObject ShortLights;

    public LightSettings LS;
    private Light LongLight1;
    private Light LongLight2;
    private Light ShortLight1;
    private Light ShortLight2;
    // Start is called before the first frame update
    void Start()
    {
        LongLight1 = LongLights.transform.GetChild(0).gameObject.GetComponent<Light>();
        LongLight2 = LongLights.transform.GetChild(1).gameObject.GetComponent<Light>();

        ShortLight1 = ShortLights.transform.GetChild(0).gameObject.GetComponent<Light>();
        ShortLight2 = ShortLights.transform.GetChild(1).gameObject.GetComponent<Light>();
        LongLightsGetSettings();
    }

    private void LongLightsGetSettings()
    {
        LongLight1.intensity = LS.LongLightIntensity;
        LongLight1.range = LS.LongLightRange;
        LongLight1.spotAngle = LS.LongLightSpotRange;

        LongLight2.intensity = LS.LongLightIntensity;
        LongLight2.range = LS.LongLightRange;
        LongLight2.spotAngle = LS.LongLightSpotRange;

        ShortLight1.intensity = LS.ShortightIntensity;
        ShortLight1.range = LS.ShortLightRange;
        ShortLight1.spotAngle = LS.ShortLightSpotRange;

        ShortLight2.intensity = LS.ShortightIntensity;
        ShortLight2.range = LS.ShortLightRange;
        ShortLight2.spotAngle = LS.ShortLightSpotRange;

    }

    // Update is called once per frame
    void Update()
    {
        GetInput();
    }

    private void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            if(LongLight1.isActiveAndEnabled && LongLight2.isActiveAndEnabled)
            {
                LongLight1.enabled = false;
                LongLight2.enabled = false;
            }else if (ShortLight1.isActiveAndEnabled && ShortLight2.isActiveAndEnabled)
            {
                ShortLight1.enabled = false;
                ShortLight2.enabled = false;               
                LongLight1.enabled = true;
                LongLight2.enabled = true;
            }
            else
            {
                ShortLight1.enabled = true;
                ShortLight2.enabled = true;
            }
        }
    }

}
