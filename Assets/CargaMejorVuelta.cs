﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class CargaMejorVuelta : MonoBehaviour
{
    public MejorVuelta MV;
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<TextMeshProUGUI>().text = "Mejor Vuelta:" + MV.minutos+":"+ +MV.segundos + ":" + MV.mil + ":";

    }
}
