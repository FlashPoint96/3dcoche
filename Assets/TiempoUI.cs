﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class TiempoUI : MonoBehaviour
{
    public MejorVuelta MV;
    public GameObject Minutos;
    public GameObject Segundos;
    public GameObject Mili;

    public int minu;
    public int seg;
    public float mil;

    public bool Empezar = false;

    public GameObject MejorMinutos;
    public GameObject MejorSegundos;
    public GameObject MejorMili;


    // Update is called once per frame
    void Update()
    {
        if (Empezar)
        {
            mil = mil + (Time.deltaTime * 10);
            Mili.GetComponent<TextMeshProUGUI>().text = "" + mil.ToString("F0");
            if (mil >= 9)
            {
                mil = 0;
                seg = seg + 1;
            }
            if (seg < 10)
            {
                Segundos.GetComponent<TextMeshProUGUI>().text = "0" + seg.ToString() + ":";
            }
            else if (seg >= 10 && seg <= 59)
            {
                Segundos.GetComponent<TextMeshProUGUI>().text = "" + seg.ToString() + ":";
            }
            else if (seg == 60)
            {
                seg = 0;
                minu = minu + 1;
            }
            if (minu < 10)
            {
                Minutos.GetComponent<TextMeshProUGUI>().text = "Tiempo Vuelta:" + minu.ToString() + ":";
            }
        }
    }


    public void EmpezarVueltas()
    {
        Empezar = true;
    }
    public void FinVuelta()
    {
        if(minu >= 1)        {
            SpeedMeter.numerovueltas++;
            if (minu < MV.minutos || 0 == MV.minutos && 0 == MV.segundos && 0 == MV.mil)
            {
                MV.minutos = minu;
                MV.segundos = seg;
                MV.mil = mil;
            }else if (minu == MV.minutos && seg < MV.segundos)
            {
                MV.minutos = minu;
                MV.segundos = seg;
                MV.mil = mil;
            }else if(minu == MV.minutos && seg == MV.segundos && mil < MV.mil)
            {
                MV.minutos = minu;
                MV.segundos = seg;
                MV.mil = mil;
            }
            else
            {
                print("nada");
            }

            mil = 0;
            seg = 0;
            minu = 0;

            MejorMili.GetComponent<TextMeshProUGUI>().text = "" + MV.mil.ToString("F0");
            if (MV.mil >= 9)
            {
                MV.mil = 0;
                MV.segundos = MV.segundos + 1;
            }
            if (MV.segundos < 10)
            {
                MejorSegundos.GetComponent<TextMeshProUGUI>().text = "0" + MV.segundos.ToString() + ":";
            }
            else if (MV.segundos >= 10 && MV.segundos <= 59)
            {
                MejorSegundos.GetComponent<TextMeshProUGUI>().text = "" + MV.segundos.ToString() + ":";
            }
            else if (MV.segundos == 60)
            {
                MV.segundos = 0;
                MV.minutos = MV.minutos + 1;
            }
            if (MV.minutos < 10)
            {
                MejorMinutos.GetComponent<TextMeshProUGUI>().text = "Mejor Vuelta:" + MV.minutos.ToString() + ":";
            }
            Empezar = false;
        }
        else
        {
            print("no ha pasado 1 minutos");
            print(minu);
            if(minu >= 1)
            {
                print("F");
            }
        }
    }
}
